FROM nginx:alpine

#INSTALAR O NGINX
RUN apk update; apk add nginx

RUN rm -rf /usr/share/nginx/html/*
WORKDIR /usr/share/nginx/html
#COPY index.html /usr/share/nginx/html/
ADD . /usr/share/nginx/html